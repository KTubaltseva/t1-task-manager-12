package ru.t1.ktubaltseva.tm.controller;

import ru.t1.ktubaltseva.tm.api.controller.ICommandController;
import ru.t1.ktubaltseva.tm.api.service.ICommandService;
import ru.t1.ktubaltseva.tm.model.Command;
import ru.t1.ktubaltseva.tm.util.FormatUtil;

public final class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void displayAbout() {
        System.out.println("[ABOUT]");
        System.out.println("name: Tubaltseva Ksenia");
        System.out.println("email: ktubaltseva@t1-consulting.ru");
    }

    @Override
    public void displayArgumentError() {
        System.err.println("Invalid argument");
        System.exit(1);
    }

    @Override
    public void displayCommandError() {
        System.err.println("Invalid command");
        displayHelp();
    }

    @Override
    public void displayHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getCommands();
        for (final Command command : commands) System.out.println(command);
        System.out.println();
    }

    @Override
    public void displaySystemInfo() {
        final Runtime runtime = Runtime.getRuntime();

        final int availableProcessors = runtime.availableProcessors();
        final long freeMemory = runtime.freeMemory();
        final long maxMemory = runtime.maxMemory();
        final long totalMemory = runtime.totalMemory();
        final long usageMemory = totalMemory - freeMemory;

        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;

        final String freeMemoryFormat = FormatUtil.formatBytes(freeMemory);
        final String maxMemoryFormat = (maxMemoryCheck ? "no limit" : FormatUtil.formatBytes(maxMemory));
        final String totalMemoryFormat = FormatUtil.formatBytes(totalMemory);
        final String usageMemoryFormat = FormatUtil.formatBytes(usageMemory);

        System.out.printf("Available processors (cores):\t %s\n", availableProcessors);
        System.out.println();
        System.out.printf("Free memory:\t %s\n", freeMemoryFormat);
        System.out.printf("Maximum memory:\t %s\n", maxMemoryFormat);
        System.out.printf("Total memory:\t %s\n", totalMemoryFormat);
        System.out.printf("Usage memory:\t %s\n", usageMemoryFormat);
        System.out.println();
    }

    @Override
    public void displayVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.12.0");
    }

    @Override
    public void exit() {
        System.out.println("[EXIT]");
        System.exit(0);
    }

}
