package ru.t1.ktubaltseva.tm.model;

import ru.t1.ktubaltseva.tm.enumerated.Status;

import java.util.UUID;

public final class Project {

    private final String id = UUID.randomUUID().toString();

    private String name;

    private String description;

    private Status status = Status.NOT_STARTED;

    public Project(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Project(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        String result = "";
        if (name != null)
            result += name;
        if (description != null)
            result += "\t(" + description + ")";
        if (status != null)
            result += "\t" + Status.toName(status) + "";
        return result;
    }

}
