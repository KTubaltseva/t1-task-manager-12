package ru.t1.ktubaltseva.tm.api.repository;

import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    Task add(Task task);

    void clear();

    Task create(String name, String description);

    Task create(String name);

    List<Task> findAll();

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

}
