package ru.t1.ktubaltseva.tm.repository;

import ru.t1.ktubaltseva.tm.api.repository.ICommandRepository;
import ru.t1.ktubaltseva.tm.constant.ArgumentConst;
import ru.t1.ktubaltseva.tm.constant.CommandConst;
import ru.t1.ktubaltseva.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    private static final Command HELP = new Command(
            CommandConst.HELP,
            ArgumentConst.HELP,
            "Display list of commands."
    );

    private static final Command VERSION = new Command(
            CommandConst.VERSION,
            ArgumentConst.VERSION,
            "Display program version."
    );

    private static final Command ABOUT = new Command(
            CommandConst.ABOUT,
            ArgumentConst.ABOUT,
            "Display developer info."
    );

    private static final Command INFO = new Command(
            CommandConst.INFO,
            ArgumentConst.INFO,
            "Display system info."
    );

    private static final Command EXIT = new Command(
            CommandConst.EXIT,
            null,
            "Close Application."
    );

    private static final Command PROJECT_CLEAR = new Command(
            CommandConst.PROJECT_CLEAR,
            null,
            "Clear project list."
    );

    private static final Command PROJECT_CREATE = new Command(
            CommandConst.PROJECT_CREATE,
            null,
            "Create new project."
    );

    private static final Command PROJECT_COMPLETE_BY_ID = new Command(
            CommandConst.PROJECT_COMPLETE_BY_ID,
            null,
            "Complete project by Id."
    );

    private static final Command PROJECT_COMPLETE_BY_INDEX = new Command(
            CommandConst.PROJECT_COMPLETE_BY_INDEX,
            null,
            "Complete project by index."
    );

    private static final Command PROJECT_CHANGE_STATUS_BY_ID = new Command(
            CommandConst.PROJECT_CHANGE_STATUS_BY_ID,
            null,
            "Change project status by Id."
    );

    private static final Command PROJECT_CHANGE_STATUS_BY_INDEX = new Command(
            CommandConst.PROJECT_CHANGE_STATUS_BY_INDEX,
            null,
            "Change project status by index."
    );

    private static final Command PROJECT_DISPLAY_BY_ID = new Command(
            CommandConst.PROJECT_DISPLAY_BY_ID,
            null,
            "Display project by Id."
    );

    private static final Command PROJECT_DISPLAY_BY_INDEX = new Command(
            CommandConst.PROJECT_DISPLAY_BY_INDEX,
            null,
            "Display project by index."
    );

    private static final Command PROJECT_LIST = new Command(
            CommandConst.PROJECT_LIST,
            null,
            "Display project list."
    );

    private static final Command PROJECT_LIST_FULL_INFO = new Command(
            CommandConst.PROJECT_LIST_FULL_INFO,
            null,
            "Display project list (full info)."
    );

    private static final Command PROJECT_REMOVE_BY_ID = new Command(
            CommandConst.PROJECT_REMOVE_BY_ID,
            null,
            "Remove project by Id."
    );

    private static final Command PROJECT_REMOVE_BY_INDEX = new Command(
            CommandConst.PROJECT_REMOVE_BY_INDEX,
            null,
            "Remove project by index."
    );

    private static final Command PROJECT_START_BY_ID = new Command(
            CommandConst.PROJECT_START_BY_ID,
            null,
            "Start project by Id."
    );

    private static final Command PROJECT_START_BY_INDEX = new Command(
            CommandConst.PROJECT_START_BY_INDEX,
            null,
            "Start project by index."
    );

    private static final Command PROJECT_UPDATE_BY_ID = new Command(
            CommandConst.PROJECT_UPDATE_BY_ID,
            null,
            "Update project by Id."
    );

    private static final Command PROJECT_UPDATE_BY_INDEX = new Command(
            CommandConst.PROJECT_UPDATE_BY_INDEX,
            null,
            "Update project by index."
    );

    private static final Command TASK_CLEAR = new Command(
            CommandConst.TASK_CLEAR,
            null,
            "Clear task list."
    );

    private static final Command TASK_CREATE = new Command(
            CommandConst.TASK_CREATE,
            null,
            "Create new task."
    );

    private static final Command TASK_COMPLETE_BY_ID = new Command(
            CommandConst.TASK_COMPLETE_BY_ID,
            null,
            "Complete task by Id."
    );

    private static final Command TASK_COMPLETE_BY_INDEX = new Command(
            CommandConst.TASK_COMPLETE_BY_INDEX,
            null,
            "Complete task by index."
    );

    private static final Command TASK_CHANGE_STATUS_BY_ID = new Command(
            CommandConst.TASK_CHANGE_STATUS_BY_ID,
            null,
            "Change task status by Id."
    );

    private static final Command TASK_CHANGE_STATUS_BY_INDEX = new Command(
            CommandConst.TASK_CHANGE_STATUS_BY_INDEX,
            null,
            "Change task status by index."
    );

    private static final Command TASK_DISPLAY_BY_ID = new Command(
            CommandConst.TASK_DISPLAY_BY_ID,
            null,
            "Display task by Id."
    );

    private static final Command TASK_DISPLAY_BY_INDEX = new Command(
            CommandConst.TASK_DISPLAY_BY_INDEX,
            null,
            "Display task by index."
    );

    private static final Command TASK_LIST = new Command(
            CommandConst.TASK_LIST,
            null,
            "Display task list."
    );

    private static final Command TASK_LIST_FULL_INFO = new Command(
            CommandConst.TASK_LIST_FULL_INFO,
            null,
            "Display task list (full info)."
    );

    private static final Command TASK_REMOVE_BY_ID = new Command(
            CommandConst.TASK_REMOVE_BY_ID,
            null,
            "Remove task by Id."
    );

    private static final Command TASK_REMOVE_BY_INDEX = new Command(
            CommandConst.TASK_REMOVE_BY_INDEX,
            null,
            "Remove task by index."
    );

    private static final Command TASK_START_BY_ID = new Command(
            CommandConst.TASK_START_BY_ID,
            null,
            "Start task by Id."
    );

    private static final Command TASK_START_BY_INDEX = new Command(
            CommandConst.TASK_START_BY_INDEX,
            null,
            "Start task by index."
    );

    private static final Command TASK_UPDATE_BY_ID = new Command(
            CommandConst.TASK_UPDATE_BY_ID,
            null,
            "Update task by Id."
    );

    private static final Command TASK_UPDATE_BY_INDEX = new Command(
            CommandConst.TASK_UPDATE_BY_INDEX,
            null,
            "Update task by index."
    );

    private static final Command[] COMMANDS = new Command[]{
            HELP,
            VERSION,
            ABOUT,
            INFO,
            EXIT,

            PROJECT_CLEAR,
            PROJECT_CREATE,
            PROJECT_COMPLETE_BY_ID,
            PROJECT_COMPLETE_BY_INDEX,
            PROJECT_CHANGE_STATUS_BY_ID,
            PROJECT_CHANGE_STATUS_BY_INDEX,
            PROJECT_DISPLAY_BY_ID,
            PROJECT_DISPLAY_BY_INDEX,
            PROJECT_LIST,
            PROJECT_LIST_FULL_INFO,
            PROJECT_REMOVE_BY_ID,
            PROJECT_REMOVE_BY_INDEX,
            PROJECT_START_BY_ID,
            PROJECT_START_BY_INDEX,
            PROJECT_UPDATE_BY_ID,
            PROJECT_UPDATE_BY_INDEX,

            TASK_CLEAR,
            TASK_CREATE,
            TASK_COMPLETE_BY_ID,
            TASK_COMPLETE_BY_INDEX,
            TASK_CHANGE_STATUS_BY_ID,
            TASK_CHANGE_STATUS_BY_INDEX,
            TASK_DISPLAY_BY_ID,
            TASK_DISPLAY_BY_INDEX,
            TASK_LIST,
            TASK_LIST_FULL_INFO,
            TASK_REMOVE_BY_ID,
            TASK_REMOVE_BY_INDEX,
            TASK_START_BY_ID,
            TASK_START_BY_INDEX,
            TASK_UPDATE_BY_ID,
            TASK_UPDATE_BY_INDEX
    };

    @Override
    public Command[] getCommands() {
        return COMMANDS;
    }

}
